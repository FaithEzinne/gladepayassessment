<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;

class EmployeeTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_read_all_employees()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();
     
        $response = $this->actingAs($user)
                         ->get('/admin/employees');

        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_create_an_employee()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $employee = Employee::factory()->make();

        $response = $this->actingAs($user)
                        ->post('/admin/employees',$employee->toArray());
            
        $response->assertRedirect('/admin/employees');

    }

    /**
     * @test
     * 
     * @return void
     */
    public function authenticated_admin_can_read_single_employee()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $employee = Employee::factory()->create();

        $response = $this->actingAs($user)
                        ->get('/admin/employees/'. $employee->id);
            
        $response->assertSee($employee->first_name)
                  ->assertSee($employee->email);

    }
    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_update_an_employee()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $employee = Employee::factory()->create();

        $employee->first_name = 'Updated employee name';

        $response = $this->actingAs($user)
                        ->put('/admin/employees/'.$employee->id,$employee->toArray());
            
        $response->assertRedirect('/admin/employees');
    }
    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_delete_an_employee()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $employee = Employee::factory()->create();

        $response = $this->actingAs($user)
                        ->delete('/admin/employees/'.$employee->id);
            
        $this->assertSoftDeleted($employee);

    }
}
