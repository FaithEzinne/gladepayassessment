<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Company;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class CompanyTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_read_all_companies()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $response = $this->actingAs($user)
                         ->get('/admin/companies');

        $response->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_create_a_company()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $company = Company::factory()->make();

        $response = $this->actingAs($user)
                        ->post('/admin/companies',$company->toArray());
            
        $response->assertRedirect('/admin/companies');

        //Storage::disk('avatars')->assertExists($file->hashName());

    }

    /**
     * @test
     * 
     * @return void
     */
    public function authenticated_admin_can_read_single_company()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $company = Company::factory()->create();

        $response = $this->actingAs($user)
                        ->get('/admin/companies/'. $company->id);
            
        $response->assertSee($company->name)
                  ->assertSee($company->email);

    }
    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_update_a_company()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $company = Company::factory()->create();

        $company->name = 'Updated Company name';

        $response = $this->actingAs($user)
                        ->put('/admin/companies/'.$company->id,$company->toArray());
            
        $response->assertRedirect('/admin/companies');

        //Storage::disk('avatars')->assertExists($file->hashName());

    }
    /**
     * @test
     *
     * @return void
     */
    public function authenticated_admin_can_delete_a_company()
    {
        $user = User::where('email', 'superadmin@admin.com')->first();

        $company = Company::factory()->create();

        $response = $this->actingAs($user)
                        ->delete('/admin/companies/'.$company->id);
            
        $this->assertSoftDeleted($company);

    }
}
