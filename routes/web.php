<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'hasAdminAccess'] ], function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'admin'])->name('admin');

    Route::resource('companies', App\Http\Controllers\CompanyController::class);

    Route::resource('employees', App\Http\Controllers\EmployeeController::class);

    Route::resource('administrators', App\Http\Controllers\AdministratorController::class);
   // Route::get('/administrators', [App\Http\Controllers\HomeController::class, 'ok']);
}); 

Route::group(['middleware' => ['auth'] ], function () {
    Route::get('/company', [App\Http\Controllers\CompanyController::class, 'userCompanyDetails']);

    Route::get('/profile', [App\Http\Controllers\UserController::class, 'index']);

    Route::group(['middleware' => ['hasCompanyAccess'] ], function () {
        Route::resource('company_employees', App\Http\Controllers\CompanyEmployeeController::class);
    
    });

}); 
