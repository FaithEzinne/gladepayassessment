<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' => 'superadmin@admin.com',
            'password' => Hash::make('password'),
            'type' => User::ADMINISTRATOR_USER,
        ]);

        $role = new RoleUser;
        $role->role_id = Role::where('name', Role::ROLE_SUPER_ADMIN)->first()->id;
        $user->role()->save($role);
    }
}
