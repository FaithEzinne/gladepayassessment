<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => Role::ROLE_SUPER_ADMIN]);
        Role::create(['name' => Role::ROLE_ADMIN]);
        Role::create(['name' => Role::ROLE_COMPANY]);
        Role::create(['name' => Role::ROLE_EMPLOYEE]);
       
    }
}
