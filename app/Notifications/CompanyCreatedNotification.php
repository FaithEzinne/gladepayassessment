<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CompanyCreatedNotification extends Notification
{
    use Queueable;

    public $company;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($company)
    {
        $this->company = $company;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello')
                    ->line($this->message())
                    ->action('Login', $this->baseUrl().'/login')
                    ->line('Thank you for using our application!');
    }

    /**
    * Get the notification message
    *
    * @return string
    */
    protected function message()
    {
        $name = $this->company->user->name();
        $email = $this->company->user->email;
        $password = $this->company->access;

        return $name .' '.'has been created with email address:' .''.$email .' ' .'and password:'.''.$password. '. '.'Kindly Login to change password';
    }

     /**
     * Get the base url for the frontend
     * 
     * @return string
     */
    protected function baseUrl()
    {
        return env('APP_URL');
        //return config('app.APP_URL');
    }
}
