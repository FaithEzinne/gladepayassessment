<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Support\Facades\Auth;

class CompanyRepository
{
    public function getAllCompanies()
    {
        $companies = Company::paginate(10);

        return $companies;
    }

    public function store($request)
    {
        $company = Company::create($request);

        return $company;
    }

    public function update($company, $request)
    {
        $company->update($request);

        //user account
        $user = User::where('type_id', $company->id)->where('type', User::COMPANY_USER)
                        ->update(['email'=> $request['email']]);

        return $company;
    }

    public function getCompanyDetails()
    {
        if(auth()->user()->type == User::EMPLOYEE_USER){
            $employee = Employee::find(auth()->user()->type_id);
           
            return $employee;
        }
    }

    public function getCompanyEmployees()
    {
        if(auth()->user()->type == User::COMPANY_USER){
            $company = Company::find(auth()->user()->type_id);
            $employees = $company->employees()->paginate(10);

            return $employees;
        }
    }

}


