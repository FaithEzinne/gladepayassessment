<?php
namespace App\Repositories;

use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;

class RoleRepository
{
    public function assignRole($user)
    {
        $role_type ='';
        switch($user->type){
            case User::ADMINISTRATOR_USER:
                $role_type = Role::ROLE_ADMIN;
                break;
            case User::COMPANY_USER:
                $role_type = Role::ROLE_COMPANY;
                break;
            case User::EMPLOYEE_USER:
                $role_type = Role::ROLE_EMPLOYEE;
                break;
        }

        $role = new RoleUser;
        $role->role_id = Role::where('name', $role_type)->first()->id;
        $user->role()->save($role);
    }
}
