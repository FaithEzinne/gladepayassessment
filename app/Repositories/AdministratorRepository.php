<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class AdministratorRepository
{
    public function getAllAdministrators()
    {
        $admins = User::where('id', '!=', auth()->user()->id)->get();
        $filtered = $admins->filter(function ($admin) {
            $role = $admin->role->role->name;
            if($role == Role::ROLE_ADMIN){
                return $admin;
            }
        });

        return $filtered->all();
    }

    public function store($request)
    {
        $company = Company::create($request);

        return $company;
    }

    public function update($company, $request)
    {
        $company->update($request);

        //user account
        $user = User::where('type_id', $company->id)->where('type', User::COMPANY_USER)
                        ->update(['email'=> $request['email']]);

        return $company;
    }

}


