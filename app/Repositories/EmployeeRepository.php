<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class EmployeeRepository
{
    public function employee($id)
    {
        $employee = Employee::find($id);

        return $employee;
    }

    public function getAllEmployees()
    {
        $employees = Employee::paginate(10);

        return $employees;
    }

    public function store($request)
    {
        $employee = Employee::create($request);

        return $employee;
    }

    public function update($employee, $request)
    {
        $employee->update($request);

        //user account
        $user = User::where('type_id', $employee->id)->where('type', User::EMPLOYEE_USER)
                        ->update(['email'=> $request['email']]);

        return $employee;
    }

}


