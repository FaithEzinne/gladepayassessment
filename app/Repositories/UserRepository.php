<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\Company;
use App\Models\Employee;
use App\Repositories\RoleRepository;
use App\Events\CompanyCreated;

class UserRepository
{
    /** 
     * RoleRepository
     * **/
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function store($request, $access)
    {
        $user = User::create($request);

        $this->roleRepository->assignRole($user);

        //send email to the company
        CompanyCreated::dispatch($user, $access);

        return $user;
    }

    public function getEmployeeProfile()
    {
        $employee = Employee::find(auth()->user()->type_id);
           
        return $employee;
    }

    public function getCompanyProfile()
    {
        $company = Company::find(auth()->user()->type_id);
           
        return $company;
    }

    public function remove($type, $typeId)
    {
        User::where('type', $type)->where('type_id', $typeId)->delete();

        return;
    }

}


