<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\CompanyCreated;
use App\Notifications\CompanyCreatedNotification;
use Illuminate\Support\Facades\Notification;

class CompanyCreatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CompanyCreated $event)
    {
        $event->user->notify(new CompanyCreatedNotification($event));
    }
}
