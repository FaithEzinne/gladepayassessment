<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = auth()->user()->role->role->name;

        switch($role){
            case Role::ROLE_COMPANY:
                return view('home');
            case Role::ROLE_EMPLOYEE:
                return view('home');
            case Role::ROLE_ADMIN:
                return view('admin.home');
            case Role::ROLE_SUPER_ADMIN:
                return view('admin.home');
        }
       // return view('home');
    }

    /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admin()
    {
        return view('admin.home');
    }

}
