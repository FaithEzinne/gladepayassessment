<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    /** 
     * UserRepository
     * **/
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        if(auth()->user()->type == User::EMPLOYEE_USER){
            return $this->employeeProfile();
        }else{
            return $this->companyProfile();
        }
        
    }

    public function employeeProfile()
    {
        $profile = $this->userRepository->getEmployeeProfile();

        return view('employee_profile')->with('profile', $profile);
    }

    public function companyProfile()
    {
        $profile = $this->userRepository->getCompanyProfile();

        return view('company_profile')->with('profile', $profile);
    }
}
