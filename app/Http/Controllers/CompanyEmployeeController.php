<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Http\Requests\CreateEmployeeRequest;
use App\Repositories\UserRepository;
use App\Repositories\EmployeeRepository;
use App\Models\User;
use App\Models\Employee;
use App\Models\Company;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CompanyEmployeeController extends Controller
{
    /** 
     * CompanyRepository, UserRepository
     * **/
    public function __construct(EmployeeRepository $employeeRepository, CompanyRepository $companyRepository, UserRepository $userRepository)
    {
        $this->employeeRepository = $employeeRepository;
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->companyRepository->getCompanyEmployees();
        
        return view('employees.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->type == User::COMPANY_USER){
            $company = Company::find(auth()->user()->type_id);
        }

        return view('employees.create')->with('company', $company);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request\CreateEmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        $input = $request->all(); 

        $employee = $this->employeeRepository->store($input);

        $access = Str::random(7);
        $input['password'] = Hash::make($access);
        $input['type'] = User::EMPLOYEE_USER;
        $input['type_id'] = $employee->id;

        $this->userRepository->store($input, $access);

        return redirect()->route('company_employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
       // return view('employees.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = $this->employeeRepository->store($id);
        if(auth()->user()->type == User::COMPANY_USER){
            $company = Company::find(auth()->user()->type_id);
        }

        return view('employees.edit')->with('company', $company)->with('employee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request\UpdateEmployeeRequest  $request
     * @param  \App\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, $id)
    {
        $input = $request->all();
        $employee = $this->employeeRepository->store($id);
        $this->employeeRepository->update($employee, $input);

        return redirect()->route('company_employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->delete();

        $this->userRepository->remove(User::COMPANY_USER, $id);

        return back();
    }
}
