<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use App\Http\Requests\CreateCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /** 
     * CompanyRepository, UserRepository
     * **/
    public function __construct(CompanyRepository $companyRepository, UserRepository $userRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
    }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->companyRepository->getAllCompanies();

        return view('admin.company.index')->with('companies', $companies);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CreateCompanyRequest  $request
     */
    public function store(CreateCompanyRequest $request)
    {
        if($request->hasFile('image')){
            $path= Storage::disk('public')->put('logos', $request->file('image'));// Store new logo
            $request->merge(['logo' => $path]);
        }

        $request->merge(['created_by' => auth()->user()->id]);
        $input = $request->all(); 

        $company = $this->companyRepository->store($input);

        $access = Str::random(7);
        $input['password'] = Hash::make($access);
        $input['type'] = User::COMPANY_USER;
        $input['type_id'] = $company->id;

        $this->userRepository->store($input, $access);

        //return redirect('/companies');
        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company $company
     */
    public function show(Company $company)
    {
        return view('admin.company.show')->with('company', $company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.company.edit')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request\UpdateCompanyRequest  $request
     * @param  \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, Company $company)
    {
        if ($request->hasFile('image')) {

            if ($profile->image) {
                if (Storage::disk('public')->exists($profile->image)) {// Delete the old photo from disk
                    Storage::disk('public')->delete($profile->image);
                }
                $path= Storage::disk('public')->put('logos', $request->file('image'));// Store new photo
                $request->merge(['logo' => $path]);    
            } else {
                $path= Storage::disk('public')->put('logos', $request->file('image'));// Store new photo
                $request->merge(['logo' => $path]);   
            }

        }
        $input = $request->all();
        $this->companyRepository->update($company, $input);

        return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $this->userRepository->remove(User::EMPLOYEE_USER, $company->id);

        $company->delete();

        return back();
    }

    /**
     * Display user company details.
     *
     */
    public function userCompanyDetails()
    {
        $employee = $this->companyRepository->getCompanyDetails();
        $company = $employee->company;
        $employees = $company->employees()->paginate(10);
        
        return view('company')->with('company', $company)
                 ->with('employees', $employees);
    }

}
