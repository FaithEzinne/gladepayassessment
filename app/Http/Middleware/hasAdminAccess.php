<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Role;

class hasAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $role = $request->user()->role->role->name;
        if($role == Role::ROLE_SUPER_ADMIN || $role == Role::ROLE_ADMIN){
            return $next($request);
        }

        $request->session()->flash('error', 'You need authorization to view that page');

        return redirect('/home');
    }
}
