<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const ADMINISTRATOR_USER = 'ADMINISTRATOR';
    const COMPANY_USER = 'COMPANY';
    const EMPLOYEE_USER = 'EMPLOYEE';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        // 'name',
        'email',
        'password',
        'type',
        'type_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->hasOne(RoleUser::class);
    }

    public function name() 
    {
        $role = $this->role->role->name;
        switch($role){
            case Role::ROLE_SUPER_ADMIN:
                return 'Super Admin';
            case Role::ROLE_ADMIN:
                return 'Admin';
            case Role::ROLE_COMPANY:
                $name = Company::find($this->type_id)->name;
                return $name;
            case Role::ROLE_EMPLOYEE:
                $employee = Employee::find($this->type_id);
                return $employee->first_name.' '.$employee->last_name;
        }
    }

}
