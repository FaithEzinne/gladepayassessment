<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const ROLE_SUPER_ADMIN = 'superadmin';
    const ROLE_ADMIN = 'admin';
    const ROLE_COMPANY = 'company';
    const ROLE_EMPLOYEE = 'employee';
    
    protected $fillable = [
        'name',
    ];
    
    public function users()
    {
        return $this->hasMany(RoleUser::class);
    }
}
