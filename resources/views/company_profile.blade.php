@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="content-header row">
        <div class="content-header-left breadcrumbs-left breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a>
            </li>
            <li class="breadcrumb-item active"> Profile
            </li>
            </ol>
        </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class=" col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <span><strong> Name:</strong> {{$profile->name}}</span><br>
                            <span><strong>Email:</strong> {{$profile->email}}</span><br>
                            <span><strong>Website:</strong> {{$profile->website}}</span><br>
                           <!-- logo -->
                           
                            <span><strong>Joined At:</strong> {{$profile->created_at}}</span><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection