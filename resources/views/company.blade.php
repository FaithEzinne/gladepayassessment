@extends('layouts.app')

@section('content')
<div class="container">
    <div class="content-header row">
        <div class="content-header-left breadcrumbs-left breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
        </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <div class="row">

                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">Company Details</div>

                                <div class="card-body">
                                    <div class="card-block">
                                       
                                        <div class="col-8">
                                            <!-- add logo -->
                                            <span><strong>Name:</strong> {{$company->name}}</span><br>
                                            <span><strong>Email:</strong> {{$company->email}}</span><br>
                                            <span><strong>Website:</strong> {{$company->website}}</span><br>
                            
                                        </div>
                                       <!-- put images of file here -->
                                    </div>
                                </div>                               

                            </div>
                        </div>

                
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <div class="row">

                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">Company's Employees</div>

                                <div class="card-body">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">First Name</th>
                                        <th scope="col">Last Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($employees) @foreach ($employees as $key => $employee)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$employee->first_name}}</td>
                                        <td>{{$employee->last_name}}</td>
                                        <td>{{$employee->email}}</td>
                                        <td>{{$employee->phone}}</td>
                                    </tr>
                                    @endforeach @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                             
            @isset($employees)
            {{ $employees->links() }}
            @endisset
                            </div>
                        </div>

                
                    </div>
                </div>

            </div>
        </div>
    </div>



</div>
@endsection
