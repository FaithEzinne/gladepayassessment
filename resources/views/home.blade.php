@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="row">
            
            <div class="col-xl-3 col-lg-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-block">
                            <div class="media">
                                <div class="media-body text-xs-left">
                                    @if(auth()->user()->role->role->name == \App\Models\Role::ROLE_COMPANY)
                                      <h3 class="teal">{{\App\Models\Employee::where('company_id', auth()->user()->type_id)->count()}}</h3>
                                    @endif
                                    @if(auth()->user()->role->role->name == \App\Models\Role::ROLE_EMPLOYEE)
                                      <h3 class="teal">{{\App\Models\Employee::where('company_id', \App\Models\Employee::find(auth()->user()->type_id)->company_id)->count()}}</h3>
                                    @endif
                                    <span>Employee</span>
                                </div>
                                <div class="media-right media-middle">
                                    <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <div class="card-block">
                            You are Welcome!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
