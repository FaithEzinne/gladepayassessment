    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-blue menu-accordion menu-shadow">
      <!-- main menu header-->
      <div class="main-menu-header">
         
      </div>
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class="{{Request::is('admin') ? 'active' : ''}} nav-item"><a href="/admin"><i class="icon-stack-2"></i><span class="menu-title">Home</span></a>
            </li>
       
            <li class="{{Request::is('admin/companies') ? 'active' : ''}} nav-item"><a href="/admin/companies"><i class="icon-slideshare"></i><span class="menu-title">Companies</span></a>
            </li>
     
      
            <li class="{{Request::is('admin/employees') ? 'active' : ''}} nav-item"><a href="/admin/employees"><i class="icon-th-large"></i><span class="menu-title">Employees</span></a>
            </li>
      

            <li class="{{Request::is('admin/administrators') ? 'active' : ''}} nav-item"><a href="/admin/administrators"><i class="icon-users2"></i><span class="menu-title">Admin</span></a>
            </li>
    
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- include includes/menu-footer-->
      <!-- main menu footer-->
    </div>
