@extends('admin.layouts.app')
@section('content')
<div class="container">
    <div class="content-header row">
        <div class="content-header-left breadcrumbs-left breadcrumbs-top col-md-6 col-xs-12">
        <div class="breadcrumb-wrapper col-xs-12">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/admin')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">All Companies
            </li>
            </ol>
        </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                <div class="text-right mb-4">
                        <a class="btn btn-primary" href="{{ route('companies.create') }}">Create new company</a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @forelse ($companies as $key => $company)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$company->name}}</td>
                                        <td>{{$company->email}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-primary" href="{{ route('companies.show', $company->id) }}">View</a>
                                                <a class="btn btn-sm btn-success" href="{{ route('companies.edit', $company->id) }}">Edit</a>
                                                <form method="POST" action="{{ route('companies.destroy', $company->id) }}" class="d-inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type='submit' class="btn btn-sm btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7" class="text-center"><em>No company added.</em></td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @isset($companies)
            {{ $companies->links() }}
            @endisset
        </div>
    </div>
</div>
@endsection
