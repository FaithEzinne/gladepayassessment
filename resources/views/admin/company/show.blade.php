@extends('admin.layouts.app')

@section('content')
    <div class="container">

    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class=" col-md-6">
                <div class="card">
                    <div class="card-body">
                    
                        <div class="card-block">
                        <div class="text-center mb-4">
                            @if($company->logo)
                                <img width="100" height="100" src="{{asset('storage/'. $company->logo)}}">
                            @else
                                <img width="100" height="100" src="{{ asset('images/company.png') }}">
                            @endif
                        </div>
                            <span><strong> Name:</strong> {{$company->name}}</span><br>
                            <span><strong>Email:</strong> {{$company->email}}</span><br>
                            <span><strong>Website:</strong> {{$company->website}}</span><br>
                           @if(auth()->user()->role->role->name == \App\Models\Role::ROLE_SUPER_ADMIN)
                           <span><strong>Created By:</strong> {{$company->createdBy->email}}</span><br>
                           @endif
                           
                            <span><strong>Created At:</strong> {{$company->created_at}}</span><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



        



    </div>
@endsection