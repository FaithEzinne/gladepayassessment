<div data-scroll-to-active="true" class="main-menu menu-fixed menu-blue menu-accordion menu-shadow">
      <!-- main menu header-->
      <div class="main-menu-header">
         
      </div>
      <!-- / main menu header-->
      <!-- main menu content-->
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class="{{Request::is('home') ? 'active' : ''}} nav-item"><a href="/home"><i class="icon-stack-2"></i><span class="menu-title">Home</span></a>
            </li>
       
          @if(auth()->user()->role->role->name == \App\Models\Role::ROLE_EMPLOYEE)
            <li class="{{Request::is('company') ? 'active' : ''}} nav-item"><a href="/company"><i class="icon-slideshare"></i><span class="menu-title">Companies</span></a>
            </li>
          @endif
      
          @if(auth()->user()->role->role->name == \App\Models\Role::ROLE_COMPANY)
            <li class="{{Request::is('company/employees') ? 'active' : ''}} nav-item"><a href="/company_employees"><i class="icon-th-large"></i><span class="menu-title">Employees</span></a>
            </li>
          @endif

            <li class="{{Request::is('profile') ? 'active' : ''}} nav-item"><a href="/profile"><i class="icon-users2"></i><span class="menu-title">Profile</span></a>
            </li>
    
        </ul>
      </div>
      <!-- /main menu content-->
      <!-- main menu footer-->
      <!-- include includes/menu-footer-->
      <!-- main menu footer-->
    </div>
